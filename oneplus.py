# https://parclytaxel.tumblr.com/post/87085714319/interpreter-for-1-in-python

#!/usr/bin/env python
# 1+ interpreter in Python
# Parcly Taxel / Jeremy Tan, 2014
# Licence is CC BY-SA 4.0 / FAL 1.3 / GNU GPLv3
import sys
from collections import deque

s, sd = deque(), {}

def oneplus(srt = None):
    if not srt:
        if len(sys.argv) != 2:
            print "Usage: " + sys.argv[0] + " [file to be executed]"
            sys.exit(1)
        inp = open(sys.argv[1], 'r')
        raw = inp.read()
        inp.close()
    else: raw = srt
    
    hm, R, bst, B, N = [], {}, deque(), 0, 0
    for c in raw:
        if c == '(':
            B += 1
            bst.append(N)
        if c == ')':
            B -= 1
            R[bst.pop()] = N
            if B < 0:
                print "Imbalanced brackets!"
                sys.exit(2)
        if c == '#' and B == 0: hm.append(N)
        N += 1
    if B != 0:
        print "Imbalanced brackets!"
        sys.exit(2)
    
    N, cmt = 0, False
    while N < len(raw):
        op = raw[N]
        if not cmt:
            if op == '1': s.append(1)
            elif op == '+': s.append(s.pop() + s.pop())
            elif op == '*': s.append(s.pop() * s.pop())
            elif op == '"': s.append(s[-1])
            elif op == '/': s.rotate(1)
            elif op == '\\': s.rotate(-1)
            elif op == '^': s.extend([s.pop(), s.pop()])
            elif op == '<': s.append(0 if s.pop() < s.pop() else 1)
            elif op == '#': N = hm[s.pop()]
            elif op == '.': s.append(abs(int(raw_input())))
            elif op == ',': s.append(ord(raw_input()[0]))
            elif op == ':': print s.pop(),
            elif op == ';': sys.stdout.write(unichr(s.pop()))
            elif op == '[': cmt = True
            elif op == '(':
                rtn = raw[N + 1:R[N]].partition('|')
                if rtn[1]: sd[rtn[0]] = rtn[2]
                oneplus(sd[rtn[0]])
                N = R[N]
            elif op == 'd' : print s
        else:
            if op == ']': cmt = False
        N += 1
if __name__ == "__main__":
    oneplus()
